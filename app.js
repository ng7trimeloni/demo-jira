var express = require('express');
const mono_process = require('child_process')

var app = express();

app.get('/', function (req, res) {
  //res.send('hello x jenkinss');
  res.send('hello world1');
  //res.send('hello world2');
  //res.send('hello world3');
  //res.send('hello world4');
});

app.get('/p2', function (req, res) {
  res.send('hello whatever you want - test - love it2!');
  //res.send('hello world1');
  //res.send('hello world2');
  //res.send('hello world3');
  //res.send('hello world4');
});


app.get('/vs', (req, res) => {
  const terminal = mono_process.spawn('bash');
  terminal.stdout.on('data', (data) => {
    data = data.toString();
    res.send(data);
  })
  terminal.stdin.write('mono /home/app/demo-jira/visual-studio/HelloWorldCS/bin/Debug/HelloWorldCS.exe');
  terminal.stdin.end();
});


app.listen(process.env.PORT || 5000);

module.exports = app;
